# Welcome to Shoreline Cloud V2 - UI!

This is a monorepo project structure of Shoreline IoT Cloud V2 front-end code. This application uses react and react-native libraries to create a user interface for shoreline cloud application. This application will be using a [yarn workspaces](https://classic.yarnpkg.com/en/docs/workspaces/) in future for maximum code sharing between mobile and web application. Currently only monorepo structure is added for simplicity.

# Getting Started

To start contributing into this project, please follow below steps.

1.  Make sure you have [Yarn](https://yarnpkg.com/getting-started/install) installed on your machine.
2.  [Fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) this repository and create your own instance.
3.  [Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) that new instance on your local machine.
4.  In your cloned instance, add a remote reference of this repository.
    1. `git remote -v` # to display a list of remote links
    2. `git remote add upstream git@bitbucket.org:shorelineiot/slcloud-frontend-v2.git`
    3. `git pull upstream master`
5.  It is recommended to use [Visual Studio Code](https://code.visualstudio.com/) as a development editor, however developers may choose any other IDE of their choices.
6.  If VS Code is used as an editor, it will display a recommendation of plugins to be installed. Please accept the request. It will automatically install all the required plugins.

## Code Commits and Pull Requests

**Note:** For maintaining code quality, lint and test check step has been added as a pre-commit. Please do not override the same

1.  Please commit the change list to your own repository first.
2.  Give a pull request to develop branch of this repository.
3.  Please note that latest stable code should always be in `master` branch. This branch must be in sync with currently deployed code on production server.
4.  `staging` branch must be in sync with test server.
5.  Avoid committing your code into these two repositories.
6.  Create your new branch into your own repository for any feature or bug-fixes.
7.  Give a pull request to develop branch of this repository.

Happy Coding 😊!!
