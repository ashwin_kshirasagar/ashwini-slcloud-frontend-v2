/**
 * Entry point for application
 * @author mahesh.kedari@shorelineiot.com
 */
import React from "react";
import { ContentPane } from "./src/framework";

export default function App() {
  return <ContentPane />;
}
