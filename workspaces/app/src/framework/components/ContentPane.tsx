import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

export class ContentPane extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>This is a Content Pane of Shoreline IoT cloud 2.0 page</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
