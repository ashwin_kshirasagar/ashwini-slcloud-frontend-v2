import React from "react";
import "./App.css";
import { ContentPane } from "./framework";

const App = () => {
  return <ContentPane />;
};

export default App;
