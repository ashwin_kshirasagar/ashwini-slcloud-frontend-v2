import React, { Component } from "react";
import logo from "../../logo.svg";

export class ContentPane extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            I am learning React. And Loving it so much{" "}
          </a>
        </header>
      </div>
    );
  }
}
